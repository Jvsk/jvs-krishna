import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { MycartComponent } from './mycart/mycart.component';

const routes: Routes = [
  {
    path:'',
    component : HomeComponent
  },
  {
    path : 'menu',
    component : MenuComponent},

{
    path : 'mycart',
    component : MycartComponent},
    

  { path: 'signup', loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignUpModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
