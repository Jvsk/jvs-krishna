import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public foodItems:any[]=[];
  public foods:any[]=[];
  
  constructor(private ser:SharedService) { }

  ngOnInit(): void {
    this.foodItems=this.ser.foodItems;
  console.log(this.foodItems);
  }

  callFun(value:any)
  {
    for(let i=0;i<this.foodItems.length;i++)
     {
       if(this.foodItems[i].name==value)
       {
         this.foods.push(this.foodItems[i]);
       }

    }

    
}
callFun1()
{
  console.log(this.foods);
  this.ser.emit<any>(this.foods);
}
}
