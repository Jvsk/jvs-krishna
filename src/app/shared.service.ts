import { Injectable } from '@angular/core';
import {BehaviorSubject,Observable} from 'rxjs';
const foodItems = [
  {name:"Chana Bhatura", price: "12.5",image:'assets/food.jpg'},
  {name: "Paneer Tikka", price: "12.5",image:'assets/food.jpg'},
  {name: "Dal Makhani", price:"14",image:'assets/food.jpg'},
  {name: "Rumali Roti", price: "4.25",image:'assets/food.jpg'},
  {name: "Aloo Fry", price: "13.5",image:'assets/food.jpg'},
  {name: "Chicke65", price: "9.5",image:'assets/food.jpg'},
  {name: "Chicken Tikka", price:"9",image:'assets/food.jpg'},
  {name: "Chicken lollipop", price: "14.25",image:'assets/food.jpg'},
  {name:"Prawn fry", price: "19.5",image:'assets/food.jpg'},
  {name: "Ginger prawn", price: "18",image:'assets/food.jpg'},
  {name: "Chicken Biryani", price:"14",image:'assets/food.jpg'},
  {name: "Veg Biryani", price: "15",image:'assets/food.jpg'},
  {name:"Mutton Biryani", price: "16",image:'assets/food.jpg'},
  {name: "Prawn Biryani", price: "12.5",image:'assets/food.jpg'},
  {name: "Dal Chawal", price:"10.4",image:'assets/food.jpg'},
  {name: "Icecream", price: "7.25",image:'assets/food.jpg'},
];

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public foodItems:any[]=foodItems;
  public foods:any[]=[];

  constructor() { }

  public subject=new BehaviorSubject<any>('');
  emit<T>(data: T)
  {
    console.log(data);
    this.subject.next(data);
  }
  on<T>(): Observable<T>{
    return this.subject.asObservable();
  }
  
}
