import { Component, OnInit } from '@angular/core';
import {ReactiveFormsModule,FormGroup,FormControl,Validators} from '@angular/forms';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor() { }

  public myform=new FormGroup({
    'name':new FormControl('',[Validators.required]),
    'contact':new FormControl('',[Validators.required,Validators.pattern('^[0-9]{10}$')]),
    'email':new FormControl('',[Validators.required])
    });
  ngOnInit(): void {
  }

}
