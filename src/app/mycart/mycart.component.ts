import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.scss']
})
export class MycartComponent implements OnInit {

  constructor( private ser:SharedService) { }
  public foods:any[]=[];

  ngOnInit(): void {

    this.ser.on<any>().subscribe(
      data=>{
        this.foods=data;
      }
    )
  }

}
